// Nama Program : stack.cpp
// NPM          : …
// Nama         : …
// Tgl buat     : 16-10-2024
// Deskripsi    : Contoh coding stack 

#include <iostream>
#define MAX_STACK 10

using namespace std;

typedef struct STACK{
    int top;
    int data[10];
};

STACK tumpuk;

void inisialisasi()
{
   tumpuk.top = -1;
}

int IsFull()
{
    if(tumpuk.top==MAX_STACK-1)
    {
        return 1;
    }else{
        return 0;
    }
}

int IsEmpty()
{
    if(tumpuk.top==-1)
    {
        return 1;
    }else{
        return 0;
    }
}

void push(int d)
{
   tumpuk.top++;
   tumpuk.data[tumpuk.top]=d;
}

void pop()
{
    cout<<"Data yang terambil = "<<tumpuk.data[tumpuk.top]<<endl;
    tumpuk.top--;
}

void print()
{
    for(int i=tumpuk.top;i>-1;i--)
    {
      cout<<"Data ke-"<<i<<" = "<<tumpuk.data[i]<<endl;
    }
}

int main()
{
    int data[15]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

    inisialisasi();

    cout<<"Masukan Data Ke Stack : "<<endl;
    int i=0;
    while(IsFull()==0)
    {
      cout<<"Masukan "<<data[i]<<" ke stack"<<endl;
      push(data[i]);
      i++;
    }

    if(IsEmpty()==0)
    {
        print();
    }

    return 0;
}
