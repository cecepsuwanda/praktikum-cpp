/* Nama Program : matrik.cpp                           * 
 * NPM          : …                                    * 
 * Nama         : …                                    *  
 * Tgl buat     : 14-10-2024                           * 
 * Deskripsi    : mengisi dan mencetak matrik          * 
 * --------------------------------------------------- */  


#include <iostream>
using namespace std;

// Definisikan tipe matriks menggunakan typedef
typedef int Tmatrik[3][3];

// Fungsi untuk mengisi elemen matriks
void isi(Tmatrik& matrik) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cout << "A[" << i + 1 << "," << j + 1 << "] = ";
            cin >> matrik[i][j];
        }
    }
}

// Fungsi untuk mencetak elemen matriks
void cetak(const Tmatrik& matrik) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cout << matrik[i][j] << " ";
        }
        cout << endl;
    }
}

int main() {
    Tmatrik matrik;  // Deklarasi matriks

    cout << "Mengisi elemen matrik A" << endl;
    isi(matrik);  // Memanggil fungsi untuk mengisi matriks

    cout << "Mencetak elemen matrik A" << endl;
    cetak(matrik);  // Memanggil fungsi untuk mencetak matriks

    return 0;
}
