// Nama Program : sort4.cpp
// NPM          : …
// Nama         : …
// Tgl buat     : 23-10-2024
// Deskripsi    : Insertion Sort 


#include <iostream>
#include <cstdlib> // For rand() and srand()
#include <ctime>   // For time()
#define MAX 30

using namespace std;

// Function to display the array
void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

// Function to perform Insertion Sort
void insertionSort(int arr[], int n) {
    for (int i = 1; i < n; i++) {
        cout<<"iterasi ke-"<<i<<" : "<<endl;
        int key = arr[i];
        cout<<"key=arr["<<i<<"] = "<<key<<endl;
        int j = i - 1;

        // Move elements of arr[0..i-1], that are greater than key,
        // to one position ahead of their current position
        while (j >= 0 && arr[j] > key) {
            cout<<"geser arr["<<j<<"]="<<arr[j]<<" ke arr[" <<j+1<<"]="<<arr[j+1]<<endl;
            arr[j + 1] = arr[j];
            printArray(arr, MAX);
            j = j - 1;
        }
        cout<<"ganti arr["<<j+1<<"]="<<arr[j+1]<<" dengan " <<key<<endl;
        arr[j + 1] = key;
        printArray(arr, MAX);
    }
}



int main() {
    int arr[MAX];
    
    // Seed the random number generator
    srand(time(0));

    // Assign 30 random numbers between 1 and 100 to the array
    for (int i = 0; i < MAX; i++) {
        arr[i] = rand() % 100 + 1;  // Random number between 1 and 100
    }

    // Display the original array
    cout << "Original array: " << endl;
    printArray(arr, MAX);

    // Perform Insertion Sort
    insertionSort(arr, MAX);

    // Display the sorted array
    cout << "Sorted array: " << endl;
    printArray(arr, MAX);

    return 0;
}
