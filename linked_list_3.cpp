#include <iostream>

using namespace std;

typedef struct TNode{
   int data;
   TNode *next;
};

TNode *head;

void init()
{
    head = NULL;
}

int isEmpty()
{
    if(head==NULL)
       return 1;
    else
       return 0;

}

void insertDepan(int databaru){
	 TNode *baru,*bantu;
     baru = new TNode;
     baru->data = databaru;
     baru->next = baru;
     if(isEmpty()==1){
        head=baru;
        head->next=head;
     }
     else {
         bantu = head;
         while(bantu->next!=head){
               bantu=bantu->next;
         }
         baru->next = head;
         head = baru;
         bantu->next = head;
  }
}

void insertBelakang (int databaru){
     TNode *baru,*bantu;
     baru = new TNode;
     baru->data = databaru;
     baru->next = baru;
     if(isEmpty()==1){
            head=baru;
            head->next=head;
     }
      else {
             bantu = head;
             while(bantu->next != head){
                   bantu=bantu->next;
             }
             bantu->next = baru;
             baru->next = head;
           }
}

void hapusDepan (){
	 TNode *hapus,*bantu;
    if (isEmpty()==0){
       hapus = head;
       if(head->next != head){
          bantu = head;
          while(bantu->next!=head){
                bantu=bantu->next;
          }
          head = head->next;
          delete hapus;
          bantu->next = head;
       }else{
           head=NULL;
       }
    }
}

void hapusBelakang(){
	TNode *hapus,*bantu;
     if (isEmpty()==0){
          hapus = head;
          if(head->next == head){
             head = NULL;
          }else{
                bantu = head;
                while(bantu->next->next != head){
                      bantu = bantu->next;
                }
                hapus = bantu->next;
                bantu->next = head;
                delete hapus;
           }

     }
}

void clear(){
	TNode *bantu,*hapus;
	bantu = head;
	while(bantu->next!=head){
		hapus = bantu;
		bantu = bantu->next;
		delete hapus;
	}
	head = NULL;
}


void tampil(){
		TNode *bantu;
		bantu = head;
		if(isEmpty()==0){
			do{
				cout<<bantu->data<<" ";
				bantu=bantu->next;
			}while(bantu!=head);
			cout << endl;
		}
	}

int main()
{
    cout << "Masukan 5 ke linked list : " << endl;
    insertDepan(5);
    tampil();
    cout << "Masukan 1 ke depan linked list : " << endl;
    insertDepan(1);
    tampil();
    cout << "Masukan 10 ke belakang linked list : " << endl;
    insertBelakang(10);
    tampil();
    cout << "Hapus depan : " << endl;
    hapusDepan();
    tampil();
    cout << "Hapus belakang : " << endl;
    hapusBelakang();
    tampil();
    clear();
    return 0;
}
