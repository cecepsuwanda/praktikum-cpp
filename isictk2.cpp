/* Nama Program : isictk2.cpp                          * 
 * NPM          : …                                    * 
 * Nama         : …                                    *  
 * Tgl buat     : …                                    * 
 * Deskripsi    : mengisi dan mencetak array           * 
 * --------------------------------------------------- */ 


#include <iostream>
using namespace std;

// Definisikan tipe array menggunakan typedef
typedef int Tarray[3];

// Fungsi untuk mengisi array
void isi(Tarray& nilai) {
    for (int i = 0; i < 3; i++) {
        cout << "Nilai ke " << i + 1 << " = ";
        cin >> nilai[i];
    }
}

// Fungsi untuk mencetak array
void cetak(const Tarray& nilai) {
    for (int i = 0; i < 3; i++) {
        cout << "Nilai ke " << i + 1 << " = " << nilai[i] << endl;
    }
}

int main() {
    Tarray nilai;  // Array untuk menyimpan nilai

    cout << "Mengisi elemen array" << endl;
    isi(nilai);  // Memanggil fungsi isi

    cout << "Mencetak elemen array" << endl;
    cetak(nilai);  // Memanggil fungsi cetak

    return 0;
}
