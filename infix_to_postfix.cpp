// Nama Program : infix_to_postfix.cpp
// NPM          : …
// Nama         : …
// Tgl buat     : 01-12-2024
// Deskripsi    : Contoh conversi infix ke postfix

#include <iostream>
#include <math.h>
#define MAX 100

using namespace std;

typedef struct
{
  int top=-1;
  char data[MAX];
}Tstack;

typedef struct
{
  int top=-1;
  int data[MAX];
}Tstack_int;

typedef struct{
    int head=-1;
    int tail=-1;
    char data[MAX];
}Tqueue;

void push(Tstack&,char);
void push_int(Tstack_int&,int);
void add(Tqueue&,char);
char pop(Tstack&);
int pop_int(Tstack_int&);
bool isEmpty_stack(Tstack);
bool isEmpty_queue(Tqueue);
void print_queue(Tqueue);

void inToPost(string,Tqueue&);
int post_eval(Tqueue);

int main()
{
    string infix;
    Tqueue postfix;
    int result;
    cout << "Enter the infix expression : ";cin>>infix;
    inToPost(infix,postfix);
    cout << "The equivalent postfix expression is : ";
    print_queue(postfix);
    result = post_eval(postfix);
    cout << "The result obtain after postfix evaluation is : " << result;
    return 0;
}

void print_queue(Tqueue postfix)
{
   for(int i=0;i<=postfix.tail;i++)
        cout << postfix.data[i];
   cout<<endl;
}

bool isEmpty_stack(Tstack stack)
{
   return stack.top==-1;
}

bool isEmpty_queue(Tqueue queue)
{
   return queue.tail==-1;
}

int precedence(char symbol)
{
    int val=0;
    switch(symbol)
    {
      case '^':val=3;
               break;
      case '/':
      case '*':val=2;
               break;
      case '+':
      case '-':val=1;
               break;
    }
    return val;
}

void push(Tstack &stack,char symbol)
{
  stack.data[++stack.top]=symbol;
}

char pop(Tstack &stack)
{
   return stack.data[stack.top--];
}

void push_int(Tstack_int &stack,int bil)
{
  stack.data[++stack.top]=bil;
}

int pop_int(Tstack_int &stack)
{
   return stack.data[stack.top--];
}

void add(Tqueue &queue,char symbol)
{
  if(isEmpty_queue(queue))
    queue.head = 0;
  queue.data[++queue.tail]=symbol;
}

void inToPost(string infix,Tqueue &postfix)
{
  Tstack stack;
  char symbol,next;
  for(int i=0;i<infix.length();i++)
  {
    symbol = infix[i];
    switch(symbol)
    {
      case '(':
               push(stack,symbol);
               break;
      case ')':
               while((next=pop(stack)) != '(' )
                     add(postfix,next);
               break;
      case '+':
      case '-':
      case '*':
      case '/':
      case '^':
               while(!isEmpty_stack(stack) && precedence(stack.data[stack.top])>=precedence(symbol))
                  add(postfix,pop(stack));
               push(stack,symbol);
               break;
      default :
               add(postfix,symbol);

    }
  }
  while(!isEmpty_stack(stack))
    add(postfix,pop(stack));
  add(postfix,'\0');

}

int post_eval(Tqueue postfix)
{
  Tstack_int stack;
  char symbol;
  for(int i=0;i<postfix.tail;i++)
  {
     symbol = postfix.data[i];
     if(symbol>='0' && symbol<='9')
     {
       push_int(stack,symbol-'0');
     }else{
        int bil1 = pop_int(stack);
        int bil2 = pop_int(stack);
        switch(symbol)
        {
          case '^':push_int(stack,pow(bil2,bil1));
                   break;
          case '/':push_int(stack,bil2/bil1);
                   break;
          case '*':push_int(stack,bil2*bil1);
                   break;
          case '+':push_int(stack,bil2+bil1);
                   break;
          case '-':push_int(stack,bil2-bil1);
                   break;
        }

     }
  }
  return pop_int(stack);
}


