#include <iostream>

using namespace std;

typedef struct TNode{
   int data;
   TNode *next;
   TNode *prev;
};

TNode *head;

void init()
{
    head = NULL;
}

int isEmpty()
{
    if(head==NULL)
       return 1;
    else
       return 0;

}

void insertDepan(int databaru){
	  TNode *baru,*bantu;
	  baru = new TNode;
	  baru->data = databaru;
	  baru->next = baru;
	  baru->prev = baru;
	  if(isEmpty()==1){
	 	head=baru;
	 	head->next = head;
	 	head->prev = head;
	  }
	  else {
		bantu = head->prev;
		baru->next = head;
		head->prev = baru;
		head = baru;
		head->prev = bantu;
		bantu->next = head;
	  }
}

void insertBelakang (int databaru){
  TNode *baru,*bantu;
  baru = new TNode;
  baru->data = databaru;
  baru->next = baru;
  baru->prev = baru;
  if(isEmpty()==1){
    head=baru;
    head->next = head;
    head->prev = head;
  }
  else {
    bantu=head->prev;
    bantu->next = baru;
    baru->prev = bantu;
    baru->next = head;
    head->prev = baru;
  }

}

void hapusDepan (){
	TNode *hapus,*bantu;
	if (isEmpty()==0){
	 if(head->next != head){
		hapus = head;
		bantu = head->prev;
		head = head->next;
		bantu->next = head;
		head->prev=bantu;
		delete hapus;
	 } else {
		head = NULL;
	 }
	}
}

void hapusBelakang(){
	TNode *hapus,*bantu;
	if (isEmpty()==0){
	 if(head->next != head){
		hapus = head->prev;
		bantu = hapus->prev;
		bantu->next = head;
        head->prev = bantu;
		delete hapus;
	 } else {
		head = NULL;
	 }
	}
}

void clear(){
	TNode *bantu,*hapus;
	bantu = head;
	while(bantu->next!=head){
		hapus = bantu;
		bantu = bantu->next;
		delete hapus;
	}
	head = NULL;
}


void tampil(){
		TNode *bantu;
		bantu = head;
		if(isEmpty()==0){
			do{
				cout<<bantu->data<<" ";
				bantu=bantu->next;
			}while(bantu!=head);
			cout << endl;
		}
	}

int main()
{
    cout << "Masukan 5 ke linked list : " << endl;
    insertDepan(5);
    tampil();
    cout << "Masukan 1 ke depan linked list : " << endl;
    insertDepan(1);
    tampil();
    cout << "Masukan 10 ke belakang linked list : " << endl;
    insertBelakang(10);
    tampil();
    cout << "Hapus depan : " << endl;
    hapusDepan();
    tampil();
    cout << "Hapus belakang : " << endl;
    hapusBelakang();
    tampil();
    clear();
    return 0;
}
