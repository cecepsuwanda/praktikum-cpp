// Nama Program : upah2.cpp
// NPM          : …
// Nama         : …
// Tgl buat     : 16-10-2024
// Deskripsi    : Menentukan upah karyawan berdasarkan 
//                golongan dan tunjangan anak

#include <iostream>
#include <string>
#include <cctype> // Untuk fungsi toupper()

using namespace std;

/* Mendefinisikan struktur untuk menyimpan data pegawai */
struct Tpegawai {
    string Nama;   // String untuk nama pegawai
    char Gol;      // Karakter untuk golongan pegawai (A - D)
    int JmlAnak;   // Jumlah anak
};

int main() {
    Tpegawai pegawai;
    float Upahkotor = 0.0, Upahbersih = 0.0;
    float PersenTunjangan = 0.0;

    /* Input data pegawai */
    cout << "Nama        : ";
    getline(cin, pegawai.Nama);  // Menggunakan getline untuk membaca nama dengan spasi

    cout << "Gol (A - D) : ";
    cin >> pegawai.Gol;
    pegawai.Gol = toupper(pegawai.Gol);  // Mengubah Gol ke huruf besar

    cout << "Jumlah Anak : ";
    cin >> pegawai.JmlAnak;

    /* Menentukan upah kotor berdasarkan golongan pegawai */
    switch (pegawai.Gol) {
        case 'A':
            Upahkotor = 1000000;
            break;
        case 'B':
            Upahkotor = 800000;
            break;
        case 'C':
            Upahkotor = 600000;
            break;
        case 'D':
            Upahkotor = 400000;
            break;
        default:
            cout << "Golongan tidak valid!" << endl;
            return 1;
    }

    /* Menentukan persentase tunjangan berdasarkan jumlah anak */
    if (pegawai.JmlAnak > 2)
        PersenTunjangan = 0.3;
    else
        PersenTunjangan = 0.2;

    /* Menghitung upah bersih */
    Upahbersih = Upahkotor - (Upahkotor * PersenTunjangan);

    /* Output upah bersih */
    cout << "Upah        : Rp " << Upahbersih << endl;

    return 0;
}
