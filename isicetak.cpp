/* Nama Program : isicetak.cpp                         * 
 * NPM          : …                                    * 
 * Nama         : …                                    *  
 * Tgl buat     : 14-10-2024                           * 
 * Deskripsi    : mengisi dan mencetak array           * 
 * --------------------------------------------------- */ 

#include <iostream>
using namespace std;

int main() {
    int nilai[3] = {0, 0, 0};  // Array dengan nilai awal 0
    int i = 0;                 // Variabel untuk iterasi dengan nilai awal 0

    // Mengisi elemen array
    cout << "Mengisi elemen array" << endl;
    while(i < 3) {
        cout << "Nilai ke " << i+1 << " = ";
        cin >> nilai[i];
        i++;
    }

    // Mencetak elemen array
    cout << "Mencetak elemen array" << endl;
    for(int i = 0; i < 3; i++) {
        cout << "Nilai ke " << i+1 << " = " << nilai[i] << endl;
    }

    return 0;
}
