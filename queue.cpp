// Nama Program : queue.cpp
// NPM          : …
// Nama         : …
// Tgl buat     : 19-10-2024
// Deskripsi    : Contoh coding queue 

#include <iostream>
#define MAX 8

using namespace std;

typedef struct Queue{
    int head;
    int tail;
    int data[MAX];
};

Queue antrian;

void create()
{
   antrian.head = -1;
   antrian.tail = -1;
}

int IsFull()
{
    if(antrian.tail==MAX-1)
    {
        return 1;
    }else{
        return 0;
    }
}

int IsEmpty()
{
    if(antrian.tail==-1)
    {
        return 1;
    }else{
        return 0;
    }
}

void enqueue(int data)
{
   if(IsEmpty()==1)
   {
       antrian.head = 0;
       antrian.tail = 0;
       antrian.data[antrian.tail]=data;
   }else
     if(IsFull()==0){
           antrian.tail++;
           antrian.data[antrian.tail]=data;
         }
}

int dequeue()
{
    int i=0;
    int e=antrian.data[antrian.head];
    for(i=antrian.head;i<=antrian.tail-1;i++)
    {
       antrian.data[i]=antrian.data[i+1];
    }
    antrian.tail--;
    return e;
}

void tampil()
{
    for(int i=antrian.head;i<=antrian.tail;i++)
    {
      cout<<"Data ke-"<<i<<" = "<<antrian.data[i]<<endl;
    }
}

int main()
{
    int data[15]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

    create();

    cout<<"Masukan Data Ke Antrian : "<<endl;
    int i=0;
    while(IsFull()==0)
    {
      cout<<"Masukan "<<data[i]<<" ke antrian"<<endl;
      enqueue(data[i]);
      i++;
    }

    if(IsEmpty()==0)
    {
        tampil();
    }

    return 0;
}
