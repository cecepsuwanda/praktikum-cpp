// Nama Program : sort2.cpp
// NPM          : …
// Nama         : …
// Tgl buat     : 23-10-2024
// Deskripsi    : Exchange Sort 

#include <iostream>
#include <cstdlib> // For rand() and srand()
#include <ctime>   // For time()
#define MAX 30

using namespace std;

void swap(int *a,int * b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

// Function to display the array
void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

// Function to perform Exchange Sort
void exchangeSort(int arr[], int n) {
    for (int i = 0; i < n-1; i++) {
        cout<<"iterasi ke-"<<i+1<<" : "<<endl;
        for (int j = i+1; j < n; j++) {
            cout<<"arr["<<i<<"]>arr["<<j<<"],"<<arr[i]<<">"<<arr[j]<<",";
            if (arr[i] > arr[j]) {
                cout<<"tukar arr["<<i<<"]="<<arr[i]<<" dengan arr["<<j<<"]="<<arr[j]<<endl;
                // Swap the elements
                swap(arr[i],arr[j]);
                cout<<"    ";
                printArray(arr, MAX);
            }else{
                cout<<"tidak ditukar"<<endl;
            }
        }
    }
}

int main() {
    int arr[MAX];
    
    // Seed the random number generator
    srand(time(0));

    // Assign 30 random numbers between 1 and 100 to the array
    for (int i = 0; i < MAX; i++) {
        arr[i] = rand() % 100 + 1;  // Random number between 1 and 100
    }

    // Display the original array
    cout << "Original array: " << endl;
    printArray(arr, MAX);

    // Perform Exchange Sort
    exchangeSort(arr, MAX);

    // Display the sorted array
    cout << "Sorted array: " << endl;
    printArray(arr, MAX);

    return 0;
}
