#include <iostream>

using namespace std;

typedef struct TNode{
   int data;
   TNode *next;
};

TNode *head,*tail;

void init()
{
    head = NULL;
    tail = NULL;
}

int isEmpty()
{
    if(tail==NULL)
       return 1;
    else
       return 0;

}

void insertDepan(int databaru){
	  TNode *baru;
      baru = new TNode;
      baru->data = databaru;
      baru->next = baru;
      if(isEmpty()==1){
         head=baru;
         tail=baru;
         head->next=head;
         tail->next=tail;
      }
      else {
           baru->next = head;
           head = baru;
           tail->next = head;
       }
}

void insertBelakang (int databaru){
     TNode *baru;
     baru = new TNode;
     baru->data = databaru;
     baru->next = baru;
     if(isEmpty()==1){
        head=baru;
        tail=baru;
        head->next=head;
        tail->next=tail;
     }
      else {
           tail->next = baru;
           tail = baru;
           tail->next = head;
    }

}

void hapusDepan(){
	TNode *hapus;
     if (isEmpty()==0){
        hapus = head;
        if(head != tail){
           hapus = head;
           head = head->next;
           tail->next = head;
           delete hapus;
          }else{
             head=NULL;
             tail=NULL;
          }
     }
}


void hapusBelakang(){
	TNode *hapus,*bantu;
     if (isEmpty()==0){
          if(head == tail){
             head = NULL;
             tail = NULL;
          }else{
              bantu = head;
              while(bantu->next != tail){
                    bantu = bantu->next;
              }
              hapus = tail;
              tail = bantu;
              tail->next = head;
              delete hapus;
          }
     }

}


void clear(){
	TNode *bantu,*hapus;
	bantu = head;
	while(bantu->next!=head){
		hapus = bantu;
		bantu = bantu->next;
		delete hapus;
	}
	head = NULL;
	tail = NULL;
}



void tampil(){
		TNode *bantu;
		bantu = head;
		if(isEmpty()==0){
			do{
				cout<<bantu->data<<" ";
				bantu=bantu->next;
			}while(bantu!=tail->next);
			cout << endl;
		}
	}

int main()
{
    cout << "Masukan 5 ke linked list : " << endl;
    insertDepan(5);
    tampil();
    cout << "Masukan 1 ke depan linked list : " << endl;
    insertDepan(1);
    tampil();
    cout << "Masukan 10 ke belakang linked list : " << endl;
    insertBelakang(10);
    tampil();
    cout << "Hapus depan : " << endl;
    hapusDepan();
    tampil();
    cout << "Hapus belakang : " << endl;
    hapusBelakang();
    tampil();
    clear();
    return 0;
}
