// Nama Program : sort3.cpp
// NPM          : …
// Nama         : …
// Tgl buat     : 23-10-2024
// Deskripsi    : Selection Sort 


#include <iostream>
#include <cstdlib> // For rand() and srand()
#include <ctime>   // For time()
#define MAX 30

using namespace std;

void swap(int *a,int * b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

// Function to display the array
void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

// Function to perform Selection Sort
void selectionSort(int arr[], int n) {
    for (int i = 0; i < n-1; i++) {
        cout<<"iterasi ke-"<<i+1<<" : "<<endl;
        // Assume the minimum is the first element
        int minIndex = i;
        cout<<"min=arr["<<minIndex<<"]="<<arr[minIndex]<<endl;
        // Find the index of the minimum element in the unsorted part
        for (int j = i+1; j < n; j++) {
            if (arr[j] < arr[minIndex]) {
                minIndex = j;
            }
        }
        cout<<"min "<<i<<" ke "<<n<<" =arr["<<minIndex<<"]="<<arr[minIndex]<<endl; 
        // Swap the found minimum element with the first element of the unsorted part
        if (minIndex != i) {
            cout<<"tukar arr["<<i<<"]="<<arr[i]<<" dengan arr["<<minIndex<<"]="<<arr[minIndex]<<endl;
            swap(arr[i],arr[minIndex]);
            printArray(arr, MAX);
        }else{
                cout<<"tidak ditukar"<<endl;
            }
    }
}



int main() {
    int arr[MAX];
    
    // Seed the random number generator
    srand(time(0));

    // Assign 30 random numbers between 1 and 100 to the array
    for (int i = 0; i < MAX; i++) {
        arr[i] = rand() % 100 + 1;  // Random number between 1 and 100
    }

    // Display the original array
    cout << "Original array: " << endl;
    printArray(arr, MAX);

    // Perform Bubble Sort
    selectionSort(arr, MAX);

    // Display the sorted array
    cout << "Sorted array: " << endl;
    printArray(arr, MAX);

    return 0;
}

