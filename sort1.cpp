// Nama Program : sort1.cpp
// NPM          : …
// Nama         : …
// Tgl buat     : 23-10-2024
// Deskripsi    : Bubble Sort 

#include <iostream>
#include <cstdlib> // For rand() and srand()
#include <ctime>   // For time()
#define MAX 30

using namespace std;

void swap(int *a,int * b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

// Function to display the array
void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

// Function to perform Bubble Sort
void bubbleSort(int arr[], int n) {
    for (int i = 0; i < n-1; i++) {
        cout<<"iterasi ke-"<<i+1<<" : "<<endl;
        for (int j = 0; j < n-1; j++) {
            cout<<"arr["<<j<<"]>arr["<<j+1<<"],"<<arr[j]<<">"<<arr[j+1]<<",";
            if (arr[j] > arr[j+1]) {
                cout<<"tukar arr["<<j<<"]="<<arr[j]<<" dengan arr["<<j+1<<"]="<<arr[j+1]<<endl;
                // Swap the elements
                swap(arr[j],arr[j+1]);
                cout<<"    ";
                printArray(arr, MAX);
            }else{
                cout<<"tidak ditukar"<<endl;
            }
        }
    }
}

int main() {
    int arr[MAX];
    
    // Seed the random number generator
    srand(time(0));

    // Assign 10 random numbers between 1 and 100 to the array
    for (int i = 0; i < MAX; i++) {
        arr[i] = rand() % 100 + 1;  // Random number between 1 and 100
    }

    // Display the original array
    cout << "Original array: " << endl;
    printArray(arr, MAX);

    // Perform Bubble Sort
    bubbleSort(arr, MAX);

    // Display the sorted array
    cout << "Sorted array: " << endl;
    printArray(arr, MAX);

    return 0;
}
