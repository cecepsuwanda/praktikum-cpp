// Nama Program : fupah3.cpp
// NPM          : …
// Nama         : …
// Tgl buat     : 16-10-2024
// Deskripsi    : Menentukan upah karyawan 
//                berdasarkan golongan dan tunjangan anak

#include <iostream>
#include <string>
#include <cctype>  // Untuk fungsi toupper()

using namespace std;

/* Mendefinisikan struktur untuk menyimpan data pegawai */
struct Tpegawai {
    string Nama;   // String untuk nama pegawai
    char Gol;      // Karakter untuk golongan pegawai (A - D)
    int JmlAnak;   // Jumlah anak
};

/* Mendefinisikan array untuk 5 pegawai */
typedef Tpegawai Tdaf_peg[5];

/* Fungsi untuk input data pegawai */
void input(Tpegawai &pegawai) {
    cout << "Nama        : ";
    cin.ignore();  // Mengabaikan karakter newline yang tertinggal
    getline(cin, pegawai.Nama);  // Membaca nama dengan spasi

    cout << "Gol (A - D) : ";
    cin >> pegawai.Gol;  // Membaca golongan pegawai
    pegawai.Gol = toupper(pegawai.Gol);  // Mengubah Gol ke huruf besar

    cout << "Jumlah Anak : ";
    cin >> pegawai.JmlAnak;  // Membaca jumlah anak
}

/* Fungsi untuk menghitung upah kotor berdasarkan golongan pegawai */
float upahkotor(char gol) {
    switch (gol) {
        case 'A':
            return 1000000;
        case 'B':
            return 800000;
        case 'C':
            return 600000;
        case 'D':
            return 400000;
        default:
            return 0;  // Kasus default untuk input tidak valid
    }
}

/* Fungsi untuk menghitung persentase tunjangan berdasarkan jumlah anak */
float persentunjangan(int jmlanak) {
    if (jmlanak > 2) {
        return 0.3;  // Tunjangan 30% jika jumlah anak lebih dari 2
    } else {
        return 0.2;  // Tunjangan 20% jika jumlah anak 2 atau kurang
    }
}

/* Fungsi untuk mencetak data pegawai dan menghitung upah bersih */
void cetak(const Tpegawai &pegawai) {
    float upahKotor = upahkotor(pegawai.Gol);
    float tunjangan = upahKotor * persentunjangan(pegawai.JmlAnak);
    float upahBersih = upahKotor - tunjangan;

    cout << "\nNama        : " << pegawai.Nama << endl;
    cout << "Gol (A - D) : " << pegawai.Gol << endl;
    cout << "Jumlah Anak : " << pegawai.JmlAnak << endl;
    cout << "Upah        : Rp " << upahBersih << endl;
}

int main() {
    Tdaf_peg daf_pegawai;  // Array untuk 5 pegawai
    int i;

    /* Input data untuk 5 pegawai */
    for (i = 0; i < 5; i++) {
        cout << "Data ke " << i + 1 << ":\n";
        input(daf_pegawai[i]);
    }

    /* Cetak data dan hitung upah bersih untuk 5 pegawai */
    for (i = 0; i < 5; i++) {
        cout << "\nData ke " << i + 1 << ":\n";
        cetak(daf_pegawai[i]);
    }

    return 0;
}
