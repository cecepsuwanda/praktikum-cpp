#include <iostream>

using namespace std;

typedef struct TNode{
   int data;
   TNode *next;
   TNode *prev;
};

TNode *head,*tail;

void init()
{
    head = NULL;
    tail = NULL;
}

int isEmpty()
{
    if(tail==NULL)
       return 1;
    else
       return 0;

}

void insertDepan(int databaru){
	  TNode *baru;
	  baru = new TNode;
	  baru->data = databaru;
	  baru->next = baru;
	  baru->prev = baru;
	  if(isEmpty()==1){
	 	head=tail=baru;
	 	head->next = head;
	 	head->prev = head;
	 	tail->next = tail;
	 	tail->prev = tail;
	  }
	  else {
		baru->next = head;
		head->prev = baru;
		head = baru;
		head->prev = tail;
		tail->next = head;
	  }
}

void insertBelakang (int databaru){
  TNode *baru,*bantu;
  baru = new TNode;
  baru->data = databaru;
  baru->next = baru;
  baru->prev = baru;
  if(isEmpty()==1){
    head=tail=baru;
    head->next = head;
    head->prev = head;
    tail->next = tail;
    tail->prev = tail;
  }
  else {
    tail->next = baru;
    baru->prev = tail;
    tail=baru;
    tail->next = head;
    head->prev = tail;
  }

}

void hapusDepan(){
	TNode *hapus,*bantu;
	if (isEmpty()==0){
		if(head!=tail){
		  hapus = head;
		  head = head->next;
		  tail->next = head;
		  head->prev = tail;
		  delete hapus;
		} else {
		  head=tail=NULL;
		}
	}
}


void hapusBelakang(){
	TNode *bantu,*hapus;
	if (isEmpty()==0){
		if(head!=tail){
			hapus = tail;
			tail=tail->prev;
			tail->next = head;
			head->prev = tail;
			delete hapus;
		}else {
			head=tail=NULL;
		}

	}
}


void clear(){
	TNode *bantu,*hapus;
	bantu = head;
	while(bantu->next!=head){
		hapus = bantu;
		bantu = bantu->next;
		delete hapus;
	}
	head = NULL;
	tail = NULL;
}



void tampil(){
		TNode *bantu;
		bantu = head;
		if(isEmpty()==0){
            do{
				cout<<bantu->data<<" ";
				bantu=bantu->next;
			}while(bantu!=tail->next);
			cout << endl;
		}
	}

int main()
{
    cout << "Masukan 5 ke linked list : " << endl;
    insertDepan(5);
    tampil();
    cout << "Masukan 1 ke depan linked list : " << endl;
    insertDepan(1);
    tampil();
    cout << "Masukan 10 ke belakang linked list : " << endl;
    insertBelakang(10);
    tampil();
    cout << "Hapus depan : " << endl;
    hapusDepan();
    tampil();
    cout << "Hapus belakang : " << endl;
    hapusBelakang();
    tampil();
    clear();
    return 0;
}
